import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokeApiService {
  private url: string  = 'https://pokeapi.co/api/v2/pokemon/?limit=100&offset=';

  constructor(
    private http: HttpClient
  ) { }

  public apiListAllPokemons(offset: number):Observable<any>
  {
    return this.http.get<any>(`https://pokeapi.co/api/v2/pokemon/?limit=10&offset=${offset * 10}`).pipe(
      tap(res => res),
      tap(res => {
        res.results.map((resPokemons: any) => {
          this.apiGetPokemons(resPokemons.url).subscribe(
            res =>  resPokemons.status = res
          )
        })
      })
    ) as Observable<any>;
  }

  public apiGetPokemons(url: string): Observable<any> {
      return this.http.get<any>(url).pipe(
        map(
          res => res
        )
      )
  }
}
