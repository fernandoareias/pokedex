import { Component, OnInit } from '@angular/core';
import { PokeApiService } from 'src/app/services/poke-api.service';

@Component({
  selector: 'app-poke-list',
  templateUrl: './poke-list.component.html',
  styleUrls: ['./poke-list.component.scss']
})
export class PokeListComponent implements OnInit {
  private setAllPokemons: Array<any> = [];
  public getAllPokemons: Array<any> = [];

  private offset: number = 0;

  constructor(private pokeApiService: PokeApiService) { }

  ngOnInit(): void {
    this.pokeApiService.apiListAllPokemons(this.offset).subscribe(res => {
      this.setAllPokemons = res.results;
      this.getAllPokemons = this.setAllPokemons;
    });
  }

  onScroll(): void {
      this.pokeApiService
      .apiListAllPokemons(++this.offset)
        .subscribe((pokemons: any) => {
          this.getAllPokemons.push(...pokemons.results);
        });
    }


  public getSearch(value: string){
    const filter = this.setAllPokemons.filter(
      (res: any) => {
        return !res.name.indexOf(value.toLowerCase());
      });

      this.getAllPokemons = filter;
  }
}
